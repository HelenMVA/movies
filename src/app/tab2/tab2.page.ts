import { Component } from "@angular/core";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page {
  KEY: string = "d139cedf";

  movieArr: Array<string> = [
    "Avengers: Endgame",
    "Joker",
    "Toy Story 4",
    "Le Roi Lion"
  ];
  newArr = [];
  movies: any;

  constructor() {}

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.getMoviesList();
  }

  getMoviesList() {
    fetch(`http://www.omdbapi.com/?apikey=${this.KEY}&s=all`).then(response => {
      response.json().then(data => {
          this.movies = data.Search;
          console.log(this.movies);
        })
    });
  }
}
